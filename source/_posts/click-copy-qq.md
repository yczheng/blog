---
title: HTML点击链接复制到剪切板
date: 2022-01-13 13:34:00
updated: 2022-03-01 01:28:01
tags: 
categories: 
- tip

---

# 前言

我在捣鼓个人博客时发现，主题中点击 <i class="fab fa-qq"></i>图标时发现，原有的网页打开QQ添加好友或者临时对话的链接不起作用了，网页打开QQ显示当前不支持。

<!--more-->

![Not Supoorted](https://s1.328888.xyz/2022/01/29/y1jJC.png)

原有代码是：

```html
<a target="_blank" href="tencent://message/?uin=<?php echo $this->options->freeLinkQQ; ?>&site=qq&menu=yes"><i class="fa fa-qq"></i></a>

<!-- Or -->

<a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=<?php echo $this->options->freeLinkQQ; ?>&site=qq&menu=yes"><i class="fa fa-qq"></i></a>
```

# 点击复制链接

现在想一个方案替换上面所述，考虑点击链接，然后复制站长QQ号到剪切板，网上搜索了若干方案，最终选择借助第三方JS库 `clickboard.js`，代码如下：

```html
<script src="https://cdn.bootcdn.net/ajax/libs/clipboard.js/2.0.8/clipboard.min.js" </script>
<a href="javascript:;" onclick="shareCopyUrl(this)" data-clipboard-text="<?php echo $this->options->freeLinkQQ; ?>"><i class="fa fa-qq"></i></a>
<script>
	function shareCopyUrl(that) {
    var clipboard = new ClipboardJS(that);
    clipboard.on('success', function (e) {
      console.info('Copy QQ:', e.text);
      alert("QQ复制成功");
      e.clearSelection();
    });

    clipboard.on('error', function (e) {
      // console.error('Action:', e.action);
      // console.error('Trigger:', e.trigger);
    });
    // 处理第一次点击不成功
    clipboard.onClick(event);
	}

</script>
```

修改后点击图标 <i class="fab fa-qq"></i>后QQ号就复制到剪切板了。

# 参考

https://blog.csdn.net/muzidigbig/article/details/117918741

<head> 
    <script defer src="https://use.fontawesome.com/releases/v5.15.4/js/all.js"></script> 
    <script defer src="https://use.fontawesome.com/releases/v5.15.4/js/v4-shims.js"></script> 
</head>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css">
