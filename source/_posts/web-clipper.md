---
title: 网页剪藏方法
date: 2022-02-06 14:53:00
updated: 2022-02-12 12:15:36
tags: 
categories: 
- tip

---

在阅读博客或其他网页时，如果遇到干货想要保存下来，以便今后查阅，如果仅保存链接（比如放入收藏夹），那么之后想起来查看时，网站可能已经下线，只能去搜索引擎的archive查找。那么最好是我们保存一份网页拷贝，这时用到的工具是网页剪藏。经过一番搜索，主要有5种方式可以实现：

1. 通用剪藏工具
2. 稍后读类应用
3. 笔记软件剪藏
4. 书签管理类
5. 浏览器自带

# 通用剪藏工具

主要是将网页保存为html或者是Markdown文件。

- [MaoXian web clipper](https://github.com/mika-cn/maoxian-web-clipper)  主要是导出到本地，目前Chrome和Edge暂时无法使用，Firefox和Chromium可以用；可以将网页保存成html或markdown格式，并在本地以文件夹的形式组织存储，图片会一并下载到本地。另在剪藏的时候，可以选择范围。
- [Save Page WE](https://chrome.google.com/webstore/detail/save-page-we/dhhpefjklgkmgeafimnjhojgjamoafof)  该插件可以将网页剪藏成html文件并存到本地，其只会生成一个html文件，图片等内容通过base64的形式存储到html中。
- [Web Clipper](https://github.com/webclipper/web-clipper)  主要是剪藏到各类笔记应用
- [Markdownload](https://github.com/deathau/markdownload)  网页解析为Markdown并下载。
- [gildas-lormeau/SingleFile](https://github.com/gildas-lormeau/SingleFile)  将网页保存为单个的html文件，提供浏览器扩展和命令行工具。
- [ArchiveBox/ArchiveBox](https://github.com/ArchiveBox/ArchiveBox)  开源的自建 Web Archiving 服务，可以保存网页为HTML，PDF，图片等格式。

# 稍后读类应用

- [cubox](https://cubox.pro/) 结合剪藏（需要Pro）的有体系的碎片知识整理工具。
- [简悦](http://ksria.com/simpread/) 非常开放，可以与诸多应用联动，以存储到坚果云、印象笔记、onenote、语雀、google driver等地方，也可导入到Pocket等；配置稍微麻烦些，价格便宜，一次性买断。
- [Pocket](https://getpocket.com/)  知名稍后读应用。
- [Instapaper](https://www.instapaper.com/)  知名稍后读应用。
- [raindrop](https://raindrop.io/)  书签管理，免费用户即可存无限标签，付费账户可存网页快照。
- [wallabag](https://www.wallabag.it/en)  代码开源，可自行服务器部署，或者托管。
- [Diigo](https://www.diigo.com/index)  可以在网页，pdf等做笔记，并同步、分享，也具有 Achive、管理书签等功能，运营时间长，年费昂贵，体验好。

# 笔记软件剪藏

下面选取近期更新频率还不错的有剪藏插件的笔记应用。

- 印象笔记 [剪藏](https://www.yinxiang.com/new/product/webclipper/)  老牌笔记应用，剪藏是其知名功能。
- OneNote [剪藏](https://www.onenote.com/clipper)  微软出品，很自由的笔记应用，搭配Onedrive很不错。
- 思源笔记 [Chrome](https://github.com/siyuan-note/siyuan-chrome)  双链笔记应用代表之一，更新活跃，剪藏效果不错。
- Notion [Chrome](https://chrome.google.com/webstore/detail/notion-web-clipper/knheggckgoiihginacbkhaalnibhilkk?hl=en)  All-in-one代表，受到很多人的喜爱。

# 书签管理类

有不少书签管理类工具，带有archive的功能。

- [Pinboard](https://pinboard.in/)  运营时间长，UI比较简陋，只有网页端，但是借口开放，有第三方客户端。网页快照额外收费。
- [dimonomid/geekmarks](https://github.com/dimonomid/geekmarks)  Go写成的，目前未更新。
- [shaarli/Shaarli](https://github.com/shaarli/shaarli/)  自建书签管理服务。
- [historious](https://historio.us/)
- [xwmx/nb](https://github.com/xwmx/nb)  完全基于终端的bookmark管理
- [Kovah/LinkAce](https://github.com/Kovah/LinkAce)  自建服务
- [go-shiori/shiori](https://github.com/go-shiori/shiori)  Golang写的书签管理，近期有新的维护者加入，恢复更新。
- [sissbruecker/linkding](https://github.com/sissbruecker/linkding)  Python写的自建书签管理服务。
- [asciimoo/omnom](https://github.com/asciimoo/omnom)  非常新的小项目，处于早期开发阶段，各方面还很原始。

# 浏览器自带

- 打印为pdf
- 保存为html，mhtml
- Safari，firefox的阅读列表等。

# 其他

- [Kiwix (github.com)](https://github.com/kiwix)  网页分享
- [Memex](https://github.com/WorldBrain/Memex)
- [tagspaces](https://github.com/tagspaces/tagspaces)  An offline, open source, document manager with tagging support.
- [Nextcloud bookmarks](https://github.com/nextcloud/bookmarks)  需要搭配nextcloud一起用
- [codever](https://github.com/codeverland/codever)  主要用于暂存代码片段

# 参考

https://renyili.org/post/backup_web_pages/

[如何优雅的保存网页到本地，就算原网页失效了，依然不影响我查看这个网页 - V2EX](https://v2ex.com/t/796366)
