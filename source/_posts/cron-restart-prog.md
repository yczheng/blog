---
title: Linux定时重启任务
date: 2022-01-17 14:39:00
updated: 2022-03-01 01:28:37
tags: 
- Linux
categories: 
- tip

---

最近想要配置一个定时任务来重启一个Golang程序，因为是在服务器上，所以决定用Linux的crontab来设置。

# Crontab

crontab是用来定期执行程序的命令。当安装完成操作系统之后，默认便会启动此任务调度命令。

crond 命令每分锺会定期检查是否有要执行的工作，如果有要执行的工作便会自动执行该工作。

具体用法网上很多，这里不再赘述。

# 结束进程

对Linux稍微有所了解便可知一般用 `kill PID`来结束特定进程，然后可能会出现类似下面的提示信息：

```
/home/user/script: line XXX: 93290 Killed       <prog>
```

如果我一次行kill多个进程，很可能因为这个输出而没能成功，因此需要去除。考虑如下脚本：

```bash
#!/bin/bash

for pid in $(pgrep -f go);
do
{ kill $pid && wait $pid; } 2>/dev/null
done

cd /root/lcid-go

nohup /usr/local/go/bin/go run ./main.go > /dev/null 2>&1 &
```

其中`$(pgrep -f go)`找出所有go程序，当然能换成其他的，只要找出所有想要结束的进程的pid。最后一行是重新启动这个go程序。

此脚本保存为`~/restart.sh`

# 设置定时任务

最后只需`crontab -e`，然后添加下面一行即可，设定为每天2:15执行。

```
15 2 * * * cd ~ && ./restart.sh
```

# 参考

https://www.runoob.com/linux/linux-comm-crontab.html

https://stackoverflow.com/questions/5719030/bash-silently-kill-background-function-process
