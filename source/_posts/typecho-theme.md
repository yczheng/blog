---
title: Typecho主题收集
date: 2022-02-28 12:36:00
updated: 2022-03-07 12:16:01
tags: 
- typecho
categories: 
- blog

---

[scode type="green"]本文将持续更新。[/scode]

Typecho是国内开发者开发的一款开源免费的轻量动态博客程序，可以运行在基于PHP环境的各种平台上，深受一众国内站长的喜爱。虽然不如Wordpress使用广泛，但Typecho仍然有不少开发者贡献主题和插件，本文做一个主题收集。

# 主题站

网上已有大佬做了Typecho的主题/插件采集站。

- [Typecho主题模板站](https://typecho.me/)
- [Typecho主题 - Npcink](https://www.npc.ink/typecho)

# 精选免费主题

有很多精美主题是免费的，下面挑出一些我看过觉得还不错的，值得一试。

- [freewind](https://github.com/kevinlu98/freewind)
- [MWordStar](https://github.com/changbin1997/MWordStar)
- [Joe](https://github.com/HaoOuBa/Joe)
- [Facile](https://github.com/changbin1997/Facile)
- [Cuckoo](https://github.com/bhaoo/Cuckoo)
- [Nexmoe](https://github.com/theme-nexmoe/typecho-theme-nexmoe)
- [Bearsimple](https://github.com/whitebearcode/typecho-bearsimple)
- [Initial](https://github.com/jielive/initial)
- [Akina](https://github.com/Zisbusy/Akina-for-Typecho)
- [sky](https://github.com/Skywt2003/sky)
- [Single](https://github.com/Dreamer-Paul/Single)
- [Echo](https://github.com/yunfeilangwu/Echo)
- [Waxy](https://github.com/dingzd1995/typecho-theme-waxy)

# 精选付费主题

同样也有不少持续维护、功能繁多的主题，有条件希望各位可以去支持。

- [handsome —— 一款typecho主题 - 友人C (ihewro.com)](https://www.ihewro.com/archives/489/)
- [Typecho主题Cuteen4.x—不止一种色彩 - 秦枫鸢梦 (zwying.com)](https://blog.zwying.com/archives/55.html)
- [Mirages - 简洁的 Typecho 主题 - Hran 的博客 (get233.com)](https://get233.com/archives/mirages-intro.html)

