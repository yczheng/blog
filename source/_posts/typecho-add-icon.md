---
title: Typecho添加icon
date: 2022-01-12 12:28:00
updated: 2022-03-01 01:27:09
tags: 
- typecho
categories: 
- blog

---

# 博客页面添加

将`favicon.ico`文件放在主题模板的根目录下， eg. `/var/www/html/usr/themes/mytheme/favicon.ico`

在模板的`header.php`的头部添加

```html
<link rel="shortcut icon" href="<?php $this->options->themeUrl('favicon.ico'); ?>" type="image/x-icon" />
```

<!--more-->

# 管理后台页面添加

后台管理界面也可以添加，如果icon来自图床或者CDN，也可以直接用icon的链接，我们需要在`/var/www/html/admin/header.php`的 `<head></head>`中添加：

```html
<link rel="shortcut icon" href="https://img.icons8.com/external-flat-juicy-fish/60/000000/external-blogging-digital-nomad-flat-flat-juicy-fish.png" type="image/x-icon" />
```

刷新页面即可在浏览器的tab看到icon。

# 参考

http://www.cnxiaocheng.top/2018/12/09/267.html

https://rabithua.club/index.php/archives/331/
