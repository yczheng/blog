---
title: Ubuntu删除旧内核
date: 2022-03-04 20:57:00
updated: 2022-03-04 21:05:17
tags: 
categories: 
- tip

---

# 方法

在使用 apt 工具升级软件或者系统之后，我会习惯性地运行命令

```bash
apt autoremove
```

来删除旧版本的软件或者依赖，以及老的系统内核，而这时老的系统内核的配置文件还在，它在系统里还能看到记录：

```bash
root@yczheng:~# dpkg --get-selections | grep linux
binutils-x86-64-linux-gnu			install
console-setup-linux				install
libselinux1:amd64				install
linux-base					install
linux-firmware					install
linux-generic					install
linux-headers-5.4.0-100				install
linux-headers-5.4.0-100-generic			install
linux-headers-5.4.0-99				install
linux-headers-5.4.0-99-generic			install
linux-headers-5.4.0-97-generic			deinstall
linux-headers-5.4.0-94-generic			deinstall
linux-headers-generic				install
linux-image-5.4.0-100-generic			install
linux-image-5.4.0-99-generic			install
linux-image-5.4.0-97-generic			deinstall
linux-image-5.4.0-94-generic			deinstall
linux-image-generic				install
linux-libc-dev:amd64				install
linux-modules-5.4.0-100-generic			install
linux-modules-5.4.0-99-generic			install
linux-modules-5.4.0-97-generic			deinstall
linux-modules-5.4.0-94-generic			deinstall
linux-modules-extra-5.4.0-100-generic		install
linux-modules-extra-5.4.0-99-generic		install
linux-modules-extra-5.4.0-97-generic		deinstall
linux-modules-extra-5.4.0-94-generic		deinstall
util-linux					install
```

其中 `deinstall` 就表示这个版本的内核已经卸载，配置文件还在。这就看着有些不爽了，我还是希望把它们删除干净（一丢丢强迫症）。当然可以直接一个一个命令删除，例如：

```bash
apt purge linux-headers-5.4.0-94-generic
```

比较麻烦，我还要复制它们的文件名，不断执行类似命令。它们的区别就是install还是deinstall，考虑一个bash脚本自动删除。

```bash
#!/bin/bash

for i in $(dpkg --get-selections | grep linux | grep deinstall | awk '{print $1}')
do
	echo "purge $i"
	apt purge $i
done

update-grub # 更新系统引导
apt autoremove
apt autoclean
```

最后更新一下系统引导，调用 `apt autoremove` 和 `apt autoclean` 清理下就好。

然后看着就清爽了:

```bash
root@yczheng:~# dpkg --get-selections | grep linux
binutils-x86-64-linux-gnu			install
console-setup-linux				install
libselinux1:amd64				install
linux-base					install
linux-firmware					install
linux-generic					install
linux-headers-5.4.0-100				install
linux-headers-5.4.0-100-generic			install
linux-headers-5.4.0-99				install
linux-headers-5.4.0-99-generic			install
linux-headers-generic				install
linux-image-5.4.0-100-generic			install
linux-image-5.4.0-99-generic			install
linux-image-generic				install
linux-libc-dev:amd64				install
linux-modules-5.4.0-100-generic			install
linux-modules-5.4.0-99-generic			install
linux-modules-extra-5.4.0-100-generic		install
linux-modules-extra-5.4.0-99-generic		install
util-linux					install
```

# 参考

[Ubuntu删除多余内核 - 简书 (jianshu.com)](https://www.jianshu.com/p/f284bc90944f)

[How to remove unused old kernel images on Ubuntu (xmodulo.com)](https://www.xmodulo.com/remove-kernel-images-ubuntu.html)
