---
title: Bash获取相对路径
date: 2022-02-18 23:21:00
updated: 2022-02-18 23:22:31
tags: 
categories: 
- tip

---

我们在Markdown或者HTML文件里可能会通过链接引用另外的文件，这时通过相对路径来引用是一个不错的选择，不会因为项目目录变更而失效。下面提供通过Bash来获取的方法。

# 方法1

realpath

```bash
realpath --relative-to="$file1" "$file2"
```

`realpath` 并不是每个Linux发行版或者MacOS都会打包的程序，于是下一个方法避免采用它。

# 方法2

```bash
#!/bin/bash
# returns $2 relative to $1

absolutepath() {
    echo "$(cd "$(dirname "$1")"; pwd)/$(basename "$1")"
}

source=$(absolutepath $1)
target=$(absolutepath $2)

if [[ -f $source ]]; then
    source=$(dirname $source)
fi 

common_part=$source
back=
while [ "${target#$common_part}" = "${target}" ]; do
    common_part=$(dirname $common_part)
    back="../${back}"
done

echo -n ${back}${target#$common_part/} | pbcopy

echo ${back}${target#$common_part/}
```

# 参考

https://stackoverflow.com/questions/2564634/convert-absolute-path-into-relative-path-given-a-current-directory-using-bash
