---
title: Github加速
date: 2022-01-05 16:46:00
updated: 2022-03-01 00:15:26
tags: 
categories: 
- tip

---

# 公共镜像

国内访问Github有时会很慢，于是网络上有很多公共镜像帮助缓存，提高Github访问速度，值得一试。

<!--more-->

```bash
# fastgit.org
https://doc.fastgit.org/

# gitclone.com
https://gitclone.com/

# gitee
https://gitee.com/mirrors

# cnpmjs.org
https://github.com.cnpmjs.org/
```

# 加速

## 克隆加速

```bash
# 原地址
git clone https://github.com/kubernetes/kubernetes.git

# 改为（推荐）
git clone https://github.com.cnpmjs.org/kubernetes/kubernetes.git

# 或者
git clone https://hub.fastgit.org/kubernetes/kubernetes.git

# 或者
git clone https://gitclone.com/github.com/kubernetes/kubernetes.git
```

具体参考镜像的使用文档

## Release加速

```bash
USER=logseq
REPO=logseq

VERSION=$(wget -qO- -t1 -T2 "https://api.github.com/repos/${USER}/${REPO}/releases/latest" | grep "tag_name" | head -n 1 | awk -F ":" '{print $2}' | sed 's/\"//g;s/,//g;s/ //g')

# 原地址
wget https://github.com/${USER}/${REPO}/releases/download/${VERSION}/Logseq-darwin-x64-${VERSION}.dmg

# 改为
wget https://hub.fastgit.org/${USER}/${REPO}/releases/download/${VERSION}/Logseq-darwin-x64-${VERSION}.dmg

```

## raw文件下载加速

```bash
# 原地址
wget https://raw.githubusercontent.com/kubernetes/kubernetes/master/README.md

# 替换为
wget https://raw.staticdn.net/kubernetes/kubernetes/master/README.md
```

# 一键免替换镜像地址

## 替换设置

可以直接在配置文件中动态替换 Github 的地址，这样不用每次克隆的时候都修改地址。

```bash
git config --global url."https://github.com.cnpmjs.org/".insteadOf "https://github.com/"
```

## 取消设置

```bash
git config --global --unset url.https://github.com/.insteadof
```

## 查看 Git 配置信息

```bash
git config --global --list
```

# 参考

https://blog.csdn.net/networken/article/details/105122778
https://www.frankfeekr.cn/2021/03/28/github-speed-up/
https://zhuanlan.zhihu.com/p/337469043
https://juejin.cn/post/6945268563259162638
