---
title: 搭建Typecho动态博客
date: 2022-01-11 21:22:00
updated: 2022-02-28 14:23:27
tags: 
- typecho
categories: 
- blog

---

# 前言

最开始我用wordpress搭建了博客，因为它的主题、插件非常丰富，功能强大，并且更新活跃。因为我买的便宜的阿里云主机只有1核2G，带宽1M，安装上后发现，单纯浏览博客还行，但是登录后台就非常卡顿。现在对PHP不了解，也没时间去研究，于是我决心换用很轻量的Typecho，最近几个月它又有了更新，所以选择pre-release的1.2版本。

<!--more-->

# 环境配置

apt先更新下。

```bash
apt update && apt upgrade
```

## sqlite3

这个博客就是用来记录下学习的琐碎内容，估计也不会有什么流量，所以数据库选最轻量的splite3绰绰有余。

```bash
apt install sqlite3
```

## PHP

Typecho 1.2默认可以在php8的环境下运行，如果安装php8的话：

```bash
apt install software-properties-common
add-apt-repository ppa:ondrej/php
apt update

apt install php8.0-fpm php8.0-sqlite3 php8.0-mbstring php8.0-curl
```

目前很多开源主题和插件在php8下可能有更多bug，用php7更妥当，如果之前添加过PPA源就先删掉，再安装php7.4：

```bash
# 如果添加过
add-apt-repository -r ppa:ondrej/php && apt update

apt install php-fpm php-sqlite3 php-mbstring php-curl
```

## Nginx

直接apt安装nginx

```bash
apt install nginx
```

然后为博客站点编辑配置文件，Ubuntu里在`/etc/nginx/sites-available/default` , 不用`default`也可以生成 新的文件，CentOS的在`/etc/nginx/conf.d/`。

还没域名和SSL证书前，测试可用：

```
server {
	listen 80 default_server;
  listen [::]:80 default_server;

  root /var/www/html;

  # Add index.php to the list if you are using PHP
  index index.php;

  server_name _;

  location / {
    # First attempt to serve request as file, then
    # as directory, then fall back to displaying a 404.
    try_files $uri $uri/ =404;
  }

  # pass PHP scripts to FastCGI server
  #
  location ~ \.php(\/.*)*$ {
    include snippets/fastcgi-php.conf;
    fastcgi_pass unix:/run/php/php-fpm.sock;
  }

  if (-f $request_filename/index.html){
  	rewrite (.*) $1/index.html break;
  }
  if (-f $request_filename/index.php){
  	rewrite (.*) $1/index.php;
  }
  if (!-e $request_filename){
  	rewrite (.*) /index.php;
  }
}
```

购买了域名（国内服务器记得ICP备案和公安备案）以及免费的SSL证书，修改nginx配置

```c
server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    server_name  yczheng.top;

    ssl_certificate /etc/nginx/ssl/yczheng.top.pem;
    ssl_certificate_key /etc/nginx/ssl/yczheng.top.key;

    root /var/www/html;
    index index.php;

    location / {
                # First attempt to serve request as file, then
                # as directory, then fall back to displaying a 404.
                try_files $uri $uri/ =404;
        }

    # pass PHP scripts to FastCGI server
    #
    location ~ \.php(\/.*)*$ {
            include snippets/fastcgi-php.conf;
            fastcgi_pass unix:/run/php/php-fpm.sock;
    }
  
    if (-f $request_filename/index.html){
            rewrite (.*) $1/index.html break;
    }
    if (-f $request_filename/index.php){
            rewrite (.*) $1/index.php;
    }
    if (!-e $request_filename){
            rewrite (.*) /index.php;
    }
}

server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    server_name www.yczheng.top;

    ssl_certificate /etc/nginx/ssl/yczheng.top.pem;
    ssl_certificate_key /etc/nginx/ssl/yczheng.top.key;

    return 301 https://yczheng.top$request_uri;
}

server {
    listen 80;
    listen [::]:80;

    server_name www.yczheng.top yczheng.top;

    return 301 https://yczheng.top$request_uri;
}
```

# 安裝Typecho

为了不因为权限问题导致网站无法运行，这里我将网站放置在目录`/var/www/html`里 ，php-fpm默认是www-data用户运行，记得给此用户赋予网站的读写权限（或者把网站文件所属赋予www-data）。

```bash
cd ~
# Download pre-release or latest from github
wget https://github.com/typecho/typecho/releases/download/v1.2.0-rc.1/typecho.zip
mv ~/typecho.zip /var/www/html/

cd /var/www/html/
unzip typecho.zip
rm typecho.zip

# php-fpm默认是www-data用户运行
chown -R www-data:www-data /var/www/html/*
```

然后在本地的浏览器，输入服务器的ip，或者域名（如果配置好），就按照提示一步一步进行就好。

# 参考

https://spinupwp.com/hosting-wordpress-yourself-setting-up-sites/

https://linuxize.com/post/how-to-install-php-8-on-ubuntu-20-04/#installing-php-80-with-nginx

https://learnku.com/php/t/51997

https://juejin.cn/post/6969113070346403853

https://developer.aliyun.com/article/764024

除此之外，很多Wordpress的建站教程也适用。
