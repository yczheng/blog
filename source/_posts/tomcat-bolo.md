---
title: Tomcat部署Bolo
date: 2022-01-02 18:15:00
updated: 2022-02-28 14:22:26
tags: 
categories: 
- blog

---

# Tomcat部署Bolo

## Bolo博客

[Bolo菠萝博客](https://github.com/adlered/bolo-solo)是基于[Solo](https://github.com/88250/solo)深度定制的动态博客，内置了更多的精美的皮肤，并且可以本地账号登录。[官网文档](https://doc.stackoverflow.wiki/web/#/7?page_id=46)提供了docker部署和tocat部署两种方式，由于我买的服务器性能有限，并便于后期修改，此次就不采用docker部署，直接安装tomcat部署web环境。

我的服务器的镜像是Alibaba Cloud Linux 3，全面兼容RHEL/CentOS 8生态。

## 安装java jdk

我选择的是openjdk8：

```bash
dnf install -y java-1.8.0-openjdk-devel
```

在`/etc/profile`配置环境变量：

```bash
JAVA_HOME=$(find /usr/lib/jvm -name "java-1.8.0-openjdk-1.8.0*")
PATH=$PATH:$JAVA_HOME/bin
CLASSPATH=.:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar
export JAVA_HOME CLASSPATH PATH
```

## 安装Tomcat

### 下载安装

Bolo需要JavaEE环境，不能选择最新的Tomcat10，需要Tomcat9或者Tomcat8。为加速下载，这里选择国内源，下载最新版的Tomcat9。

```bash
MAJOR_VERSION=9
TOMCAT_VER=`curl --silent http://mirrors.cnnic.cn/apache/tomcat/tomcat-${MAJOR_VERSION}/ | grep v${MAJOR_VERSION} | awk '{split($5,c,">v") ; split(c[2],d,"/") ; print d[1]}'`
TOMCAT_VER=$(echo $TOMCAT_VER | cut -d " " -f 1)
echo Tomcat version: $TOMCAT_VER
wget -N http://mirrors.cnnic.cn/apache/tomcat/tomcat-${MAJOR_VERSION}/v${TOMCAT_VER}/bin/apache-tomcat-${TOMCAT_VER}.tar.gz
```

接下来解压并把tomcat拷贝到`/usr/local/`目录下

```bash
tar -zxvf apache-tomcat-${TOMCAT_VER}.tar.gz
mv apache-tomcat-${TOMCAT_VER} /usr/local/tomcat/
```

### 设置服务脚本（Optional）

参考[阿里云文档](https://help.aliyun.com/document_detail/172784.html)设置自启动脚本。

1. 运行以下命令下载Tomcat自启动脚本文件。

```bash
wget https://raw.githubusercontent.com/oneinstack/oneinstack/master/init.d/Tomcat-init
```

2. 运行以下命令移动并重命名Tomcat-init。

```bash
mv Tomcat-init /etc/init.d/tomcat
```

3. 运行以下命令为/etc/init.d/tomcat添加可执行权限。

```bash
chmod +x /etc/init.d/tomcat
```

4. 设置脚本里的`JAVA_HOME`和`TOMCAT_USER`，因为我的服务器直接用的root用户，所以tocat也设置root用户

```bash
SED_CMD="s@^export JAVA_HOME=.*@export JAVA_HOME=$(find /usr/lib/jvm -name 'java-1.8.0-openjdk-1.8.0*')@"
sed -i "$SED_CMD" /etc/init.d/tomcat
sed -i "/TOMCAT_USER=/cTOMCAT_USER=root" /etc/init.d/tomcat
```

5. 依次运行以下命令设置Tomcat开机自启动。

```bash
chkconfig --add tomcat
chkconfig tomcat on
```

6. 运行以下命令启动Tomcat。

```bash
service tomcat start
```

## 安装MySQL

### 安装

```bash
dnf install -y mysql-server
```

运行以下命令启动MySQL服务。

```bash
systemctl start mysqld
```

运行以下命令设置MySQL服务开机自启动。

```bash
systemctl enable mysqld
```

运行以下命令查看/var/log/mysqld.log文件，获取并记录root用户的初始密码。

```bash
grep 'temporary password' /var/log/mysqld.log
```

运行下列命令对MySQL进行安全性配置。

```bash
mysql_secure_installation
```

### 配置博客所用MySQL DB和User

根据上面安全性配置中所设定的root账户密码，在终端中输入`mysql -uroot -p`，并输入密码以root账户登录MySQL后，手动建库，库名`solo` ，字符集使用`utf8mb4`，排序规则`utf8mb4_general_ci`

```bash
create database solo default character set utf8mb4 collate utf8mb4_general_ci;
create user 'solo'@'%' identified by 'BrkBpb_78Vkf9';
grant all privileges on *.* to 'solo'@'%';
flush privileges;
```

这里‘%’是允许远程ip地址登录MySQL，如果选用docker部署会用到。

## 部署Bolo博客

从Bolo官方Github网站下载已经编译好的代码（zip文件），[github](https://github.com/adlered/bolo-solo/releases)或者国内的[gitee](https://gitee.com/AdlerED/bolo-solo/releases)

```bash
wget https://gitee.com/AdlerED/bolo-solo/attach_files/760605/download/bolo_v2_5_stable.zip
```

删掉tomcat目录的`webapp`下的其他内容，只留空的`ROOT`

```bash
rm -rf /usr/local/tomcat/webapps/*
mkdir -p /usr/local/tomcat/webapps/ROOT
```

将Bolo的zip文件解压至目录`webapp/ROOT`下

```bash
unzip bolo_v2_5_stable.zip -d /usr/local/tomcat/webapps/ROOT/
```

在Bolo的`WEB-INF/classes`目录下修改参数

```bash
cd /usr/local/tomcat/webapps/ROOT/WEB-INF/classes/
vim local.properties
```

修改MySQL的连接参数

```bash
runtimeDatabase=MYSQL
jdbc.username=solo
jdbc.password=BrkBpb_78Vkf9
jdbc.driver=com.mysql.cj.jdbc.Driver
jdbc.URL=jdbc:mysql://127.0.0.1:3306/solo?useUnicode=yes&characterEncoding=UTF-8&useSSL=false&serverTimezone=UTC
```

如果Tomcat使用 HTTPS 连接（不使用请忽略），将`webapps/ROOT/WEB-INF/classes/latke.properties`中的 `HTTP` 修改为 `HTTPS` 即可；

## 启动应用

如果前面设置了tomcat服务脚本，可

```bash
service tomcat start
```

如果没有，用tomcat的`bin`目录下的脚本

```bash
./usr/local/tomcat/bin/startup.sh
```

然后如果一切没有问题，在本地浏览器访问服务器的8080端口，就能进入博客。

```
http://${ip}:8080
```

## 参考

https://help.aliyun.com/document_detail/172784.html
https://help.aliyun.com/document_detail/171940.html
https://cnxiaobai.com/articles/2021/04/27/1619519011942.html
