---
title: Typecho添加文章版权申明
date: 2022-02-21 16:10:36
updated: 2022-02-21 16:10:36
tags: 
- 博客美化
categories: 
- blog

---

# 前言

原创文章、代码等作品往往是一个内容创作者付出一番心血后的所得，作者们希望通过作品或是获得经济收益，或是增强自身影响力，交到更多朋友，这都无可非议。互联网的环境下复制粘贴也就是敲两次键盘的事情，版权容易受到侵犯，更有甚者白嫖得理直气壮，收到投诉指责还反唇相讥。虽然一个君子协议不能从根本上解决问题，但也希望能启示大众，重视版权。

开源领域有很多协议，文章自然也有，在网上公开文章的作者往往采用[Creative Commons](https://creativecommons.org/licenses/?lang=zh) 里的协议，读者可根据自身需要选择。

接下来介绍两种我在网上收集的能在Typecho动态博客中采用的，给文章自动添加版权声明的方法。

# 插件

经过测试，Typecho最新版本（1.2.0-rc2）能使用的相关插件是[Yves-X/Copyright-for-Typecho](https://github.com/Yves-X/Copyright-for-Typecho)，正常安装后稍作简单设置即可，并且可利用自定义字段功能灵活地在文章或独立页面尾部显示特定的版权小尾巴。

# 修改主题

## 修改 `post.php` 文件

在自己使用的主题里，找到 `post.php` 文件，在文章内容后的你喜欢的位置，添加以下html代码：

```html
<!--知识共享许可协议-->
<div class="tt-license">
    <p><span class="tt-license-icon"><i data-feather="award"></i></span>本文标题：<?php $this->title() ?></p>
    <p><span class="tt-license-icon"><i data-feather="link"></i></span>本文链接：<?php $this->permalink() ?></p>
    <p><span class="tt-license-icon"><i data-feather="shield"></i></span>除非另有说明，本作品采用<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">知识共享署名-非商业性使用-相同方式共享 4.0 国际许可协议</a>。</p>
    <p><span class="tt-license-icon"><i data-feather="alert-circle"></i></span>声明：转载请注明文章来源。</p>
</div>
<!-- / 知识共享许可协议-->
```

## 修改CSS样式

然后在主题设置的自定义CCS（若有），或者主题引用的CCS样式文件里，加上以下代码：

```css
/*文章正文下的知识共享许可协议*/
.tt-license {font-size: 12px;font-weight: 600;padding: 1rem;background: repeating-linear-gradient(135deg,#f6f6f6,#f6f6f6 12px,#fff 0,#fff 24px);background-color: #f3f5f7;border-left: 3px solid #dde6e9;margin-bottom: 20px;}
.tt-license-icon {align-items: center;position: relative;float: left;margin: -10px -10px -10px 0;margin-right: 10px;overflow: hidden;text-align: center;display: flex;height: 40px;color: #ff5722;}
.tt-license a {color: #337ab7;text-decoration: underline;margin: 0 5px;}
/*深色模式下的知识共享许可协议*/
html.theme-dark .tt-license {background: repeating-linear-gradient(135deg,#191919,#191919 12px,#222 0,#222 24px);border-left: 3px solid #494949;}.tt-license p {line-height: 1.5em;margin: 5px 0!important;}

```

效果可参考本文末尾。如果不满意，也可自行修改html以及css里添加的内容。

# 参考

[Typecho|Handsome 主题的一些修改教程 - Kid's blog (moekid.com)](https://www.moekid.com/archives/22/)

[Typecho 在文章底部添加版权说明 - WQT's Blog (wuqintai.com)](https://www.wuqintai.com/archives/2074.html)

[【博客优化】愿诸位都是君子——为自己的博文设置版权声明 - 日常见闻录 (hyouka.club)](http://www.hyouka.club/index.php/archives/29/)
